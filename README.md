CorrectHorseBatteryStaple
=========================

Password generator, loosely (read: entirely) based on XKCD 936

[http://correcthorsebatterystaple.net](http://correcthorsebatterystaple.net "http://correcthorsebatterystaple.net")


## Deployment
To deploy this, simply serve the content in this repository with a web server of
your choosing.

### Docker
Under `deploy`, there is a Dockerfile which builds this project into an
easy-to-use Dockerfile. Simply build and run the resulting image, passing
traffic from the host to port 80 on the container, and this will run.
